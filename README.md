# SHARE FILE WITH CHUNKING BUFFER
Usually, we upload a file that pushes an original size file to the server, so what happens if that file's so heavy? Usually, the server will reject the request after several seconds (usually 30s). With this simple project, I think this is another solution that can be applied to the above problem, that we should split the size file to many 1MB (2MB, 3MB,..) sizes and push them to the server. With this, the server will not reject your request. Because of using that we can share files as fast as possible between 2 clients through chunking and socketIO


## **Require packages**

### BACK END 

1. nodemon
2. express
3. socket.io

### FRONT END

1. socket.io.js (cdn)



## **HOW TO RUN**
#### 1. Install npm 
> npm install


#### 2. Start back end express server
> npm run dev


#### 3. OPEN 2 BROWSER
1. One browser goes to sender url [http://localhost:5000/sender](http://localhost:5000/sender)
2. One browser goes to receiver url [http://localhost:5000/receiver](http://localhost:5000/receiver)
3. From **SENDER** click "CREATE ROOM" to generate a number randomly.
4. AFTER step 3 go to **RECEIVER** pass that number to JOIN ID text-input.
5. FROM **SENDER** browser file and see a result.

