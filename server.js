const express = require('express');
const path = require('path');
require('dotenv').config();

const host = process.env.HOST;
const port = process.env.PORT;

const app = express();
const server = app.listen(port);
const io = require('socket.io')(server)


app.use(express.static(path.join(__dirname + "/public")));

io.on('connect', (socket) => {

  console.log('connecting from client')

  socket.on('data', () => {
    console.log('testng')
  })

  socket.on('sender-join', (data) => {
    console.log('sender join', data) 
    socket.join(data.uid)
  })

  socket.on('receiver-join', (data) => {
    console.log('uid', data.uid)
    console.log('sender', data.sender_uid);
    socket.join(data.uid)
    socket.in(data.sender_uid).emit('init', data.uid);
  })


  socket.on('file-meta', (data) => {
    socket.in(data.uid).emit('fs-meta', data.metadata)
  })

  socket.on('file-raw', (data) => {
    socket.in(data.uid).emit('fs-share', data.buffer);
  })

  socket.on('fs-start', (data) => {
    socket.in(data.uid).emit('fs-share', {})
  })


  socket.on('disconnect', (data) => {
    console.log('disconnection socket')
  })  
})


app.get('/sender', (req,res) => res.sendFile(path.join(__dirname + '/public/index.html')));

app.get('/receiver', (req, res) => res.sendFile(path.join(__dirname + '/public/receiver.html')));


app.listen(port, host, () => {
  console.log(`Server is running on port: ${port} - host: ${host}`)
})